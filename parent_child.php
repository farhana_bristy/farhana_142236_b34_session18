<?php

    class BITM{
        public $window;
        public $door;
        public $chair;
        public $table;
        public $whiteboard;

        public function coolTheAir()
        {
            echo 'I m cooling the air'.'<br>';
        }
        public function compute()
        {
            echo "I'm computing".'<br>';
        }
        public function setChair($localChair)
        {

            $this->chair = $localChair. " [ set by parent]" ;
        }

        public function setDoor($door)
        {
            $this->door = $door;
        }

        public function setWindow($win)
        {
            $this->window = $win;
        }

        public function setTable($tab)
        {
            $this->table = $tab;
        }

        public function setWhiteboard($board)
        {
            $this->whiteboard = $board;
        }
        public function show()
        {
            echo $this->window.'<br>';
            echo $this->door.'<br>';
            echo $this->chair.'<br>';
            echo $this->table.'<br>';
            echo $this->whiteboard.'<br>';
        }
    }

    class BITM_Lab402 extends BITM
    {
        public function setChairFromChild($childChair)
        {
            $childChair=$childChair.'[ set by child ]';
            parent::setChair($childChair);
        }
    }



$parentObj=new BITM();
$parentObj->compute();

$parentObj->setChair("I'm a chair");
$parentObj->setTable("I'm a Table");
$parentObj->setWindow("I'm a Window");
$parentObj->setDoor("I'm a Door");
$parentObj->setWhiteboard("I'm a Whiteboard");
$parentObj->show();

$childObject=new BITM_Lab402();
$childObject->setChair('this is a chair');
$childObject->show();

echo $childObject->chair .'<br>';
$childObject->setChairFromChild('ami ekti chair');
echo $childObject->chair;

?>